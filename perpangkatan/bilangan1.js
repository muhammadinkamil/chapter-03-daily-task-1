// bilangan 1 merupakan angka yang akan dipangkatkan
function hasilPrintBilangan1(bilangan1) {
  // menggunakan fungsi pasrseInt agar data yang diinputkan bertipe number
  return "bilangan pertama " + parseInt(bilangan1);
}

// export function
module.exports = hasilPrintBilangan1;
