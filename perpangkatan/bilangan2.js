// bilangan 2 yaitu pangkatnya
function hasilPrintBilangan2(bilangan2) {
  // menggunakan fungsi pasrseInt agar data yang diinputkan bertipe number
  return "bilangan Kedua " + parseInt(bilangan2);
}

// export function
module.exports = hasilPrintBilangan2;
