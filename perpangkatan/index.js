// install readline module
var readline = require("readline");

// import file dengan function di dalamnya
const printPangkat = require("./bilangan1.js");
const printPangkat2 = require("./bilangan2.js");

// memakai readline module
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Masukkan bilangan pertama : ", (bilangan1) => {
  const hasilPrintBilangan1 = printPangkat(bilangan1);
  console.log(hasilPrintBilangan1);

  rl.question("Masukkan bilangan kedua : ", (bilangan2) => {
    const hasilPrintBilangan2 = printPangkat2(bilangan2);
    console.log(hasilPrintBilangan2);

    // Perhitungan pangkat menggunakan fungsi Math.pow
    const jumlah = Math.pow(bilangan1, bilangan2);
    console.log(bilangan1 + " pangkat " + bilangan2 + " = " + jumlah);
    rl.close();
  });
});
